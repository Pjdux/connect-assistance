import StoryWrapper from "../styled-components/StoryWrapper";
import React, { useState, useEffect } from "react";
import CarForm from "../DOM-components/forms/CarForm";
import FormWrap from ".././styled-components/FormWrap";
let today = new Date();

let date =
  today.getMonth() + 1 + "-" + today.getDate() + "-" + today.getFullYear();
import StoryWraper from "../styled-components/StoryWrapper";
import Card from "@material-ui/core/Card";
import Box from "@material-ui/core/Box";
import { Paper } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import StoryButton from "../DOM-components/Buttons/StoryButton";
import GhButton from "../DOM-components/GhButton";
import fetch from "isomorphic-unfetch";
import ContactForm from "./ContactForm";

const AgileThoughtLogo = "https://d.cra.pr/images/connect_logo.png";

const Story = ({ posts }) => {
  const [ghData, setGhData] = useState({});
  const [moreData, setMoreData] = useState([]);

  useEffect(() => {
    const url = "https://api.github.com/users/pjdaze";
    fetch(url)
      .then((res) => res.json())
      .then((data) => setGhData({ ...data }));
  }, []);

  const getGhData = () => {
    setMoreData(
      <div>
        <img src={ghData.avatar_url} alt="" />
        <p>Name: {ghData.name} </p>
        I've got{" "}
        <span className="repo-num">
          <a href="https://github.com/Pjdaze?tab=repositories">
            {ghData.public_repos}
          </a>
        </span>{" "}
        {""}
        repos.
        <span>
          <p>You can check em Out Here</p>
          <a href={"https://github.com/Pjdaze?tab=repositories"}>
            Repos Page
          </a>{" "}
          / <a href={ghData.repos_url}>Raw</a>
        </span>
      </div>
    );
    console.log("clicked", ghData);
  };

  return (
    <StoryWrapper>
      <a className="rant" href="#assesment">
        Just Show Me
        <br />
        The Form!
      </a>
      <span id="allegis-logo">
        <img
          src={AgileThoughtLogo}
          alt="alleguis original logo provided by their website"
        />
      </span>

      <h1>
        Job Application <br /> <span id="my-name">Pedro Diaz</span>
      </h1>

      <span className="divider" />

      <div className="intro-box">
        <div>
          <p>
            Hello Guys! Pedro Diaz Here, Fisrt of all i want to thank you for
            taking the time to talk to me, i really loved the energy and i love
            the direction you guys are taking when it comes to development. Even
            though i wasn't able to complete the project on time i know i can do
            a good job with minor hand holding, just in the beggining, to get
            going with company trends and processes
            <br /> <br />
            I'm absolutely passionate about Javascript and CSS and have been
            focusing on React.js as my front-end library of choice, and Node.js
            for backend solutions. <br /> <br />
            I'm Proficient in a CSS pre-processors including SASS, LESS, and I'm
            familiar databases like MySQL MongoDB and FaunaDB.
            <br />
            <br />
            When Using React.js , I use Styled-Components as my go-to because of
            all the great features, also if needed, I have no problem using
            Semantic-UI, (my favorite), Material-UI or Bootstrap ect.
            <br />
            before and after our video call, i was taking a look into Next.js
            since i've seen it pop up quite a lot lately so decided to give it a
            try for this little app.
          </p>
          <hr style={{ opacity: "0.5" }} />
        </div>
      </div>
      <span className="divider" variant="middle" />
      <div className="intro-box">
        <Box style={{ margin: "100px  auto" }}>
          <h2>Early Realizations</h2>
          <h3>Styling In Next...</h3>
          <p>
            So right of the bat, I learned the apparently Next.js likes
            Material-UI. <br />
            My background is more of create-react-app, add react-router,
            styled-components, maybe add in Semantic-Ui as a quick prototype
            tool, so I stuck to that for this example.
          </p>
        </Box>
        <i>Here some links that help me relize it.</i>
        <div className="button-box flex-center">
          <StoryButton href="https://medium.com/javascript-in-plain-english/ssr-with-next-js-styled-components-and-material-ui-b1e88ac11dfa">
            Manato Kuroda Blog
          </StoryButton>
          <StoryButton href="https://nextjs.org/learn/basics/styling-components">
            Next.js Docks / Styling Components
          </StoryButton>
        </div>
        <hr style={{ opacity: "0.5" }} />
        <Box style={{ margin: "100px  auto" }}>
          <h2>Data Fetching</h2>
          <h3>nextjs dataAPI</h3>
          <p>
            To Keep the app server side ready, I'm using isomorphic-unfetch
            since it switches between unfetch & node-fetch for client & server.
            <br />
            <br />
          </p>

          <div className="github-data">
            <div>{moreData}</div>

            <GhButton setOnClick={getGhData} />
          </div>
        </Box>
      </div>
      <span className="divider" variant="middle" />

      <div className="intro-box">
        <Box style={{ margin: "100px  auto" }}>
          <h3>Connect Assistance Assesment(car form)</h3>
          <p>
            I believe this is what the challenge was about.. conditionally
            displaying select data depending on previous select value <br />
            <br />
            When we spoke i was confused as to what the form should do and i
            take full responability for it.
            <br />
            So for the form i'm using react select for easy access to values and
            im displaying the make and model differently depending on the
            current selected option value.
            <br />
            Check the repo for this entire project{" "}
            <a href="https://gitlab.com/Pjdux/connect-assistance">Here</a>
          </p>

          <h3 id="assesment">Connect Assistance Assessment(Car Form)</h3>
          <CarForm />
          <h2>Contact Me</h2>

          <p>
            <a href="https://pjdux.com">My Site</a> or at{" "}
            <a
              href="mailto:pjdazeux@gmail.com?Subject=Hello%20there"
              target="_top"
            >
              pjdazeux@gmail.com
            </a>
          </p>
        </Box>
      </div>
    </StoryWrapper>
  );
};

export default Story;
