import React from "react";
import FooterWrapper from "../styled-components/FooterWrapper";
import FlyImg from "../ui-images/fly-img.png";
import Nav from "./Nav";

const Footer = () => {
  return (
    <FooterWrapper>
      <footer className="footer">
        <Nav />
        <p>
          Created On The{" "}
          <a href="https://pjdux.com">
            <img src={FlyImg} alt="fly-joke" />
          </a>
        </p>
      </footer>
    </FooterWrapper>
  );
};

export default Footer;
