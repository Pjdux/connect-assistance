import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Menu from "@material-ui/core/Menu";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Link from "@material-ui/core/Link";
import NavWrapper from "../styled-components/NavWrapper";

const useStyles = makeStyles({
  list: {
    width: "90%",
    maxWidth: "500px",
    margin: "0 auto",
    display: "flex",
    justifyContent: "center",
  },
  Link: {
    fontSize: "calc(12px + 0.5vw)",
  },
});

export default function Nav() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (side, open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  const fullList = (side) => (
    <NavWrapper
      className={classes.fullList}
      role="presentation"
      onClick={toggleDrawer(side, false)}
      onKeyDown={toggleDrawer(side, false)}
    >
      <List className={classes.list}>
        {[
          <Link className={classes.Link} href="https://pjdux.com">
            <a>My Site</a>
          </Link>,
          <Link className={classes.Link} href="https://github.com/pjdaze">
            <a>My Github</a>
          </Link>,
        ].map((text, index) => (
          <ListItem button key={text}>
            <ListItemText primary={text} />
          </ListItem>
        ))}
        {[
          <Link className={classes.Link} href="/">
            <a>Home</a>
          </Link>,
          <Link className={classes.Link} href="/About">
            <a>About</a>
          </Link>,
        ].map((link, index) => (
          <ListItem button key={link}>
            <ListItemText primary={link} />
          </ListItem>
        ))}
      </List>
    </NavWrapper>
  );

  return (
    <div>
      <Button onClick={toggleDrawer("bottom", true)}>Navigate</Button>

      <Menu
        anchor="bottom"
        open={state.bottom}
        onClose={toggleDrawer("bottom", false)}
        onOpen={toggleDrawer("bottom", false)}
      >
        {fullList("bottom")}
      </Menu>
    </div>
  );
}
