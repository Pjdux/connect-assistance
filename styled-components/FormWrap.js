import styled from "styled-components";

const FormWrap = styled.form`
  margin: 0 auto;
  width: 400px;
  color: #222;
  label {
    color: #fff;
  }
  input {
    padding: 5px;
    border-radius: 4px;
  }
  .select {
    color: #222;
  }
  .phone {
    color: #222;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    margin: 5px auto;
  }
`;

export default FormWrap;
