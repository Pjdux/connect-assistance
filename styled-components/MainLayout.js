import styled from "styled-components";

const MainLayout = styled.footer`
  font-family: "Roboto", sans-serif;
`;

export default MainLayout;
