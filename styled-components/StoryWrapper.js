import styled from "styled-components";

const StoryWraper = styled.div`
  .rant {
    color: #222;
    text-shadow: 2px 2px 2px #222;
    position: fixed;
    font-size: 19px;

    right: 60px;
  }
  padding: 35px;
  margin: 100px auto;
  width: 90%;
  max-width: 900px;
  box-shadow: 0 0 10px 2px #888;
  text-align: center;

  background: #f9f9f9;

  #allegis-logo {
    img {
      width: 115px;
    }
  }
  h1 {
    color: #09487b;
    font-family: "Source Sans Pro", sans-serif;
    font-size: 1.5em;
    #my-name {
      font-weight: 300;
      font-size: 0.8em;
    }
  }
  h4 {
    margin: 45px auto;
  }

  h2 {
    font-family: "Source Sans Pro", sans-serif;
    color: #1369ab;
    font-weight: 600;
  }

  h3 {
    color: #2f1728;
    font-weight: 300;
    text-decoration: underline #ffb612;

    padding: 5px;
    font-family: "M PLUS Rounded 1c", sans-serif;
  }
  .divider {
    height: 1.5px;
  }
  .intro-box {
    width: 95%;
    max-width: 850px;
    text-align: center;
    margin: 40px auto;
    line-height: 1.5em;
    p {
      margin: 30px auto;
      font-size: calc(14px + 0.5vw);
      font-family: "M PLUS Rounded 1c", sans-serif;
      font-weight: 300;
      #long-story {
        color: #951f41;
        font-weight: bold;
      }
    }
  }

  .button-box {
    @media only screen and (min-width: 960px) {
      flex-direction: row;
    }
  }
  .github-data {
    img {
      width: 240px;
      border-radius: 50%;
    }
  }

  .repo-num {
    padding: 7px;
    background-color: #a4a59f;
    font-size: 15px;
    border-radius: 50%;
    margin-left: 7px;
    a {
      color: #222;
    }
  }
`;

export default StoryWraper;
