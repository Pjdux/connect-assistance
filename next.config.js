const withImages = require("next-images");
require("dotenv").config();

module.exports = {
  env: {
    SENDGRID_API_KEY: process.env.SENDGRID_API_KEY
  }
};
module.exports = withImages();
