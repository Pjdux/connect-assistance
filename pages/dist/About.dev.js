"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true,
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _core = require("@material-ui/core");

var _FooterWrapper = _interopRequireDefault(
  require("../styled-components/FooterWrapper")
);

var _Footer = _interopRequireDefault(require("../containers/Footer"));

var _StoryWrapper = _interopRequireDefault(
  require("../styled-components/StoryWrapper")
);

import React from "react";
var __jsx = React.createElement;

var About = function About() {
  return __jsx(
    _StoryWrapper["default"],
    {
      className: "flex-center",
    },
    __jsx(
      "h2",
      null,
      "Made with the sole purpose of Applying Connect Assistance"
    ),
    __jsx(_FooterWrapper["default"], null, __jsx(_Footer["default"], null))
  );
};

var _default = About;
exports["default"] = _default;
