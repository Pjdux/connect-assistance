import React from "react";

import { Container } from "@material-ui/core";
import FooterWrapper from "../styled-components/FooterWrapper";
import Footer from "../containers/Footer";
import StoryWraper from "../styled-components/StoryWrapper";

const About = () => {
  return (
    <StoryWraper className="flex-center">
      <h2>Made with the sole purpose of Applying Connect Assistance</h2>
      <FooterWrapper>
        <Footer />
      </FooterWrapper>
    </StoryWraper>
  );
};

export default About;
