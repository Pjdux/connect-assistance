import Story from "../containers/Story";
import Footer from "../containers/Footer";
import MainLayout from "../styled-components/MainLayout";

const Index = () => {
  return (
    <MainLayout id="main-layout">
      <Story />

      <Footer />

      <style jsx global>{`
        @import url("https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400|Open+Sans:300,300i,400,400i,600|Roboto:100i,300|Pacifico|Titillium+Web:300|Titan+One|Fascinate+Inline|Monoton&display=swap");

        * {
          box-sizing: border-box;
          margin: 0;
          padding: 0;
        }

        html {
          font-size: 18px;

        }
        @media screen and (min-width: 320px) {
          html {
            font-size: calc(16px + 6 * ((100vw - 320px) / 680));
          }
        }
        @media screen and (min-width: 900px) and (max-width: 2045px) {
          html {
            font-size: 21px;
          }
        }
        @media screen and (min-width: 2046px) {
          html {
            font-size: 1.8em;
          }
        }
        html,
        body {
          
          height: 100%;
          line-height: 1.15; /* 1 */
          -ms-text-size-adjust: 100%; /* 2 */
          -webkit-text-size-adjust: 100%; /* 2 */
          font-family: "Lato", sans-serif;
        }

        body,
        #root {
          height: 100%;
          min-height: 100vh;
        }

        a {
          text-decoration: none;
        }

        ul {
          list-style: none;
        }

        /* Sections
   ========================================================================== */

        /**
 * Add the correct display in IE 9-.
 */

        article,
        aside,
        footer,
        header,
        nav,
        section {
          display: block;
        }

        h1 {
          font-size: 2em;
          margin: 0.67em 0;
        }

        #P {
          background: #222;
        }

        .flex-center {
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        /* Grouping content
   ========================================================================== */

        /**
 * Add the correct display in IE 9-.
 * 1. Add the correct display in IE.
 */

        figcaption,
        figure,
        main {
          /* 1 */
          display: block;
        }

        /**
 * Add the correct margin in IE 8.
 */

        figure {
          margin: 1em 40px;
        }

        /**
 * 1. Add the correct box sizing in Firefox.
 * 2. Show the overflow in Edge and IE.
 */

        hr {
          box-sizing: content-box; /* 1 */
          height: 0; /* 1 */
          overflow: visible; /* 2 */
        }

        pre {
          font-family: monospace, monospace; /* 1 */
          font-size: 1em; /* 2 */
        }

        /* Text-level semantics


        .contact-form {
          max-width: 360px;
          margin: 0 auto;
          display: flex;
          flex-direction: column;
          padding: 16px;
        }

        .contact-form > :not(:last-child) {
          margin-bottom: 16px;
        }

        .contact-form .form-field {
          display: flex;
          flex-direction: column;
        }

        .contact-form .label-content {
          margin-bottom: 4px;
        }

        .contact-form {
          input,
          textarea {
            display: flex;
            flex: 1;
            width: 100%;
            max-width: 100%;
            padding: 10px;
            border: none;
            background-image: none;
            background-color: transparent;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: 0 0 0 1px #fff;

            border-radius: 5px;
          }

          textarea {
            height: 150px;
            max-height: 170px;
          }
        }

        .contact-form label {
          text-align: left;
          font-size: 19px;
        }

        a {
          background-color: transparent; /* 1 */
          -webkit-text-decoration-skip: objects; /* 2 */
        }

        /**
 * 1. Remove the bottom border in Chrome 57- and Firefox 39-.
 * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.
 */

        abbr[title] {
          border-bottom: none; /* 1 */
          text-decoration: underline; /* 2 */
          text-decoration: underline dotted; /* 2 */
        }

        b,
        strong {
          font-weight: inherit;
        }

        /**
 * Add the correct font weight in Chrome, Edge, and Safari.
 */

        b,
        strong {
          font-weight: bolder;
        }

        code,
        kbd,
        samp {
          font-family: monospace, monospace; /* 1 */
          font-size: 1em; /* 2 */
        }

        /**
 * Add the correct font style in Android 4.3-.
 */

        dfn {
          font-style: italic;
        }

        /**
 * Add the correct background and color in IE 9-.
 */

        mark {
          background-color: #ff0;
          color: #000;
        }

        /**
 * Add the correct font size in all browsers.
 */

        small {
          font-size: 80%;
        }

        sub,
        sup {
          font-size: 75%;
          line-height: 0;
          position: relative;
          vertical-align: baseline;
        }

        sub {
          bottom: -0.25em;
        }

        sup {
          top: -0.5em;
        }

        /* Embedded content
   ========================================================================== */

        /**
 * Add the correct display in IE 9-.
 */

        audio,
        video {
          display: inline-block;
        }

        /**
 * Add the correct display in iOS 4-7.
 */

        audio:not([controls]) {
          display: none;
          height: 0;
        }

        /**
 * Remove the border on images inside links in IE 10-.
 */

        img {
          border-style: none;
        }

        /**
 * Hide the overflow in IE.
 */

        svg:not(:root) {
          overflow: hidden;
        }

        /* Forms
   ========================================================================== */

        /**
 * Remove the margin in Firefox and Safari.
 */

        button,
        input,
        optgroup,
        select,
        textarea {
          margin: 0;
        }

        /**
 * Show the overflow in IE.
 * 1. Show the overflow in Edge.
 */

        button,
        input {
          overflow: visible;
        }

        button,
        select {
          /* 1 */
          text-transform: none;
        }

        button,
html [type="button"], /* 1 */
[type="reset"],
[type="submit"] {
          -webkit-appearance: button; /* 2 */
        }

        /**
 * Remove the inner border and padding in Firefox.
 */

        button::-moz-focus-inner,
        [type="button"]::-moz-focus-inner,
        [type="reset"]::-moz-focus-inner,
        [type="submit"]::-moz-focus-inner {
          border-style: none;
          padding: 0;
        }

        /**
 * Restore the focus styles unset by the previous rule.
 */

        button:-moz-focusring,
        [type="button"]:-moz-focusring,
        [type="reset"]:-moz-focusring,
        [type="submit"]:-moz-focusring {
          outline: 1px dotted ButtonText;
        }

        legend {
          box-sizing: border-box; /* 1 */
          color: inherit; /* 2 */
          display: table; /* 1 */
          max-width: 100%; /* 1 */
          padding: 0; /* 3 */
          white-space: normal; /* 1 */
        }

        progress {
          display: inline-block; /* 1 */
          vertical-align: baseline; /* 2 */
        }

        /**
 * Remove the default vertical scrollbar in IE.
 */

        textarea {
          overflow: auto;
        }

        /**
 * 1. Add the correct box sizing in IE 10-.
 * 2. Remove the padding in IE 10-.
 */

        [type="checkbox"],
        [type="radio"] {
          box-sizing: border-box; /* 1 */
          padding: 0; /* 2 */
        }

        /**
 * Correct the cursor style of increment and decrement buttons in Chrome.
 */

        [type="number"]::-webkit-inner-spin-button,
        [type="number"]::-webkit-outer-spin-button {
          height: auto;
        }

        /**
 * 1. Correct the odd appearance in Chrome and Safari.
 * 2. Correct the outline style in Safari.
 */

        [type="search"] {
          -webkit-appearance: textfield; /* 1 */
          outline-offset: -2px; /* 2 */
        }

        /**
 * Remove the inner padding and cancel buttons in Chrome and Safari on macOS.
 */

        [type="search"]::-webkit-search-cancel-button,
        [type="search"]::-webkit-search-decoration {
          -webkit-appearance: none;
        }

        ::-webkit-file-upload-button {
          -webkit-appearance: button; /* 1 */
          font: inherit; /* 2 */
        }

        /* Interactive
   ========================================================================== */

        /*
 * Add the correct display in IE 9-.
 * 1. Add the correct display in Edge, IE, and Firefox.
 */

        details, /* 1 */
menu {
          display: block;
        }

        /*
 * Add the correct display in all browsers.
 */

        summary {
          display: list-item;
        }

        /* Scripting
   ========================================================================== */

        /**
 * Add the correct display in IE 9-.
 */

        canvas {
          display: inline-block;
        }

        /**
 * Add the correct display in IE.
 */

        template {
          display: none;
        }

        /* Hidden
   ========================================================================== */

        /**
 * Add the correct display in IE 10-.
 */

        [hidden] {
          display: none;
        }
      `}</style>
    </MainLayout>
  );
};

export default Index;
