import React from "react";

import styled from "styled-components";
const StoryButton = styled.a`
  margin: 20px;
  border: 2px solid #09487b;
  border-radius: 7px;
  font-size: 0.8em;
  padding: 20px;
  font-family: "Roboto", sans-serif;
  text-decoration: none;
  color: #09487b;
`;

export default StoryButton;
