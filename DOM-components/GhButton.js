import Button from "../styled-components/Button";

function GhButton(props) {
  return <Button onClick={props.setOnClick}>Fetch Github Data</Button>;
}

export default GhButton;
