"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _reactSelect = _interopRequireDefault(require("react-select"));

var _FormWrap = _interopRequireDefault(require("../../styled-components/FormWrap"));

import React from "react";
var __jsx = React.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var initialFormData = Object.freeze({
  FirstName: "",
  LastName: ""
});

var TestForm2 = function TestForm2() {
  var _useState = (0, _react.useState)(initialFormData),
      formData = _useState[0],
      setFormData = _useState[1];

  var handleChange = function handleChange(e) {
    setFormData(_objectSpread({}, formData, (0, _defineProperty2["default"])({}, e.target.name, e.target.value.trim())));
  };

  var handleChange2 = function handleChange2(name, selectedOptions) {
    console.log("selectedOptions", selectedOptions);
    setFormData(_objectSpread({}, formData, (0, _defineProperty2["default"])({}, name, selectedOptions.label)));
  };

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    console.log(formData); // ... submit to API or something
  };

  var options = [{
    value: "2019",
    label: "2019"
  }, {
    value: "2020",
    label: "2020"
  }];
  var modelOptions = [{
    value: "Acura",
    label: "Acura"
  }, {
    value: "Toyota",
    label: "Toyota"
  }, {
    value: "BMW",
    label: "BMW"
  }];
  var makerOptions2 = {
    model2020: [{
      value: "Acura",
      label: "Acura TLX"
    }, {
      value: "Acura",
      label: "Acura RDX"
    }, {
      value: "ToyotaNew",
      label: "Toyota Yaris"
    }],
    model2019: [{
      value: "Toyota",
      label: "Toyota Camry"
    }, {
      value: "BMW",
      label: "BMW 2 Series"
    }, {
      value: "BMW",
      label: "BMW 3 series"
    }]
  }; // const filteredOptions = makerOptions.filter(
  //   (x) => x.value === "ToyotaNew" || x.Acura === "Acura"
  // );
  // const filteredOptions2 = makerOptions.filter(
  //   (x) => x.value === "Toyota" || x.BMW === "BMW"
  // );

  console.log("formData", formData);
  return __jsx(_FormWrap["default"], {
    className: "App-header",
    onSubmit: handleSubmit
  }, __jsx("div", {
    className: "phone"
  }, __jsx("h3", null, "A Little About Your Self"), " ", __jsx("br", null), __jsx("input", {
    type: "text",
    placeholder: "First Name",
    name: "FirstName",
    onChange: handleChange
  }), __jsx("input", {
    type: "text",
    placeholder: "Last Name",
    name: "LastName",
    onChange: handleChange
  }), __jsx("div", {
    className: "phone"
  }, " ", __jsx("input", {
    type: "text",
    placeholder: "Phone Number",
    name: "phone",
    onChange: handleChange
  }))), __jsx("h3", null, "Tell Us About Your Car"), __jsx("br", null), __jsx(_reactSelect["default"], {
    name: "year",
    options: options,
    onChange: function onChange(selected) {
      return handleChange2("year", selected);
    },
    style: {
      width: "300px"
    }
  }), formData.year && formData.year === "2020" && __jsx(_reactSelect["default"], {
    name: "model2020",
    onChange: function onChange(selected) {
      return handleChange2("model2020", selected);
    },
    options: makerOptions2["model2020"]
  }), formData.year && formData.year === "2019" && __jsx(_reactSelect["default"], {
    name: "model2019",
    onChange: function onChange(selected) {
      return handleChange2("model2019", selected);
    },
    options: makerOptions2["model2019"]
  }), __jsx("input", {
    type: "text",
    placeholder: "Color",
    name: "color",
    onChange: handleChange
  }), __jsx("input", {
    type: "text",
    placeholder: "Plate",
    name: "plate",
    onChange: handleChange
  }), __jsx("input", {
    type: "submit"
  }));
};

var _default = TestForm2;
exports["default"] = _default;