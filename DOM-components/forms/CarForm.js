import React, { Component, useState } from "react";
import {
  modelCarOptions,
  makerCarOptions,
  year,
} from "./options/selectOptions";

import Select from "react-select";
import FormWrap from "../../styled-components/FormWrap";
const initialFormData = {};
const TestForm2 = () => {
  const [formData, setFormData] = useState(initialFormData);
  const [displayForm, setDisplayForm] = useState([]);

  const handleInputChange = (e) => {
    setFormData({
      ...formData,

      [e.target.name]: e.target.value.trim(),
    });
  };

  const handleSelections = (name, selectedcarOptions) => {
    //console.log("selectedcarOptions", selectedcarOptions);
    setFormData({ ...formData, [name]: selectedcarOptions.label });
    //console.log("this is the maker", formData.maker);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    console.log(formData);
    // ... submit to API or something
  };

  //console.log("formData", formData);
  return (
    <FormWrap className="App-header" onSubmit={handleSubmit}>
      <div className="phone">
        <h3>A Little About Your Self</h3> <br />
        <input
          type="text"
          placeholder="First Name"
          name="FirstName"
          onChange={handleInputChange}
        />
        <input
          type="text"
          placeholder="Last Name"
          name="LastName"
          onChange={handleInputChange}
        />
        <div className="phone">
          {" "}
          <input
            type="text"
            placeholder="Phone Number"
            name="phone"
            onChange={handleInputChange}
          />
        </div>
      </div>
      <h3>Tell Us About Your Car</h3>

      <br />

      <Select
        instanceId="year"
        name="year"
        options={year}
        onChange={(selected) => handleSelections("year", selected)}
        style={{ width: "300px" }}
      />
      {formData.year && formData.year === "2020" && (
        <Select
          instanceId="maker2020"
          name="maker2020"
          onChange={(selected) => handleSelections("maker2020", selected)}
          options={makerCarOptions["maker2020"]}
        />
      )}
      {formData.year && formData.year === "2019" && (
        <Select
          instanceId="maker2019"
          name="maker2019"
          onChange={(selected) => handleSelections("maker2019", selected)}
          options={makerCarOptions["maker2019"]}
        />
      )}

      {formData.maker2020 === "Acura" && formData.year === "2020" && (
        <Select
          instanceId="model2020"
          name="model2020"
          onChange={(selected) => handleSelections("model2020", selected)}
          options={modelCarOptions["model2020"]["acura2020"]}
        />
      )}

      {formData.maker2020 === "Toyota" && formData.year === "2020" && (
        <Select
          instanceId="model2020"
          name="model2020"
          onChange={(selected) => handleSelections("model2020", selected)}
          options={modelCarOptions["model2020"]["toyota2020"]}
        />
      )}
      {formData.maker2019 === "BMW" && formData.year === "2019" && (
        <Select
          instanceId="maker2019"
          name="model2019"
          onChange={(selected) => handleSelections("model2020", selected)}
          options={modelCarOptions["model2019"]["bmw2019"]}
        />
      )}
      {formData.maker2019 === "Toyota" && formData.year === "2019" && (
        <Select
          instanceId="maker2019"
          name="model2019"
          onChange={(selected) => handleSelections("model2020", selected)}
          options={modelCarOptions["model2019"]["toyota2019"]}
        />
      )}
      <input
        type="text"
        placeholder="Color"
        name="color"
        onChange={handleInputChange}
      />
      <input
        type="text"
        placeholder="Plate"
        name="plate"
        onChange={handleInputChange}
      />
      <input type="submit" />
    </FormWrap>
  );
};

export default TestForm2;
