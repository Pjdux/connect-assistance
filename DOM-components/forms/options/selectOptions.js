const year = [
  { value: "2019", label: "2019" },
  { value: "2020", label: "2020" },
];
const makerCarOptions = {
  maker2019: [
    { value: "Toyota", label: "Toyota" },
    { value: "BMW", label: "BMW" },
  ],

  maker2020: [
    { value: "Acura", label: "Acura" },
    { value: "Toyota", label: "Toyota" },
  ],
};

const modelCarOptions = {
  model2020: {
    acura2020: [
      { value: "Acura", label: "Acura TLX" },
      { value: "Acura", label: "Acura RDX" },
    ],
    toyota2020: [{ value: "Toyota", label: "Toyota Yaris" }],
  },
  model2019: {
    toyota2019: [{ value: "Toyota", label: "Toyota Camry" }],
    bmw2019: [
      { value: "BMW", label: "BMW 2 Series" },
      { value: "BMW", label: "BMW 3 series" },
    ],
  },
};

module.exports = {
  year,
  makerCarOptions,
  modelCarOptions,
};
